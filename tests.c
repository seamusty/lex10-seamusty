#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "CUnit.h"
#include "Automated.h"
#include "calculate.h"

void test11(int expected, int actual);
/* Runs a test input that *should* pass and produce an int result
 */
void test01(void) {
  int expected = 100;
  int actual = calculate(13000,0,1);
  testDisplay( expected , actual );
}

void test02(void){ // below combined deductions
  int expected = 0;
  int actual = calculate(10,1,1);
  testDisplay( expected , actual );
}

void test03(void){ 
  int expected = 0;
  int actual = calculate(10,1,0);
  testDisplay( expected , actual ); 
}

void test04(void){
  int expected = 0;
  int actual = calculate(10,0,1);
  testDisplay( expected, actual );
}

void test05(void){
  int expected = 3200;
  int actual = calculate(32000,1,1);
  testDisplay( expected, actual );
}

void test06(void){
  int expected = 3200;
  int actual = calculate(32000,1,0);
  testDisplay( expected, actual );
}

void test07(void){
  int expected = 3200;
  int actual = calculate(32000,0,1);
  testDisplay( expected, actual);
}

void test08(void){
  int expected = 6300;
  int actual = calculate(56000,1,1);
  testDisplay( expected, actual );
}

void test09(void){
  int expected = 3900;
  int actual = calculate(56000,1,0);
  testDisplay( expected, actual ); 
}

void test10(void){
  int expected = 2700;
  int actual = calculate(38000,0,1);
  testDisplay( expected, actual ); 
}

test11(void){
  int expected = 0;
  int actual = calculate(2000,0,0);
  testDisplay( expected, actual );
}

void testDisplay(int expected, int actual){
  CU_ASSERT_EQUAL( expected , actual );
}

/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite Suite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   Suite = CU_add_suite("Suite_of_tests_with_valid_inputs", NULL, NULL);
   if (NULL == Suite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to Suite */
   if (
          (NULL == CU_add_test(Suite, "13000/0/1", test01))
        ||(NULL == CU_add_test(Suite, "test02", test02))
        ||(NULL == CU_add_test(Suite, "test03", test03))
        ||(NULL == CU_add_test(Suite, "test04", test04))
        ||(NULL == CU_add_test(Suite, "test05", test05))
        ||(NULL == CU_add_test(Suite, "test06", test06))
        ||(NULL == CU_add_test(Suite, "test07", test07))
        ||(NULL == CU_add_test(Suite, "test08", test08))
        ||(NULL == CU_add_test(Suite, "test09", test09))
        ||(NULL == CU_add_test(Suite, "test10", test10))
        ||(NULL == CU_add_test(Suite, "test11", test11))
      )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using automated interface, with output to 'test-Results.xml' */
   CU_set_output_filename("test");
   CU_automated_run_tests();

   CU_cleanup_registry();
   return CU_get_error();
}
